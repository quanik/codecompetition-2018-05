﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace GameOfLife
{
    /// <summary>
    /// Represents a Game of Life game.
    /// </summary>
    public class GOLGame
    {
        private CellGrid grid;
        // Saves the initial configuration for reset.
        private bool[,] initialConfig;

        /// <summary>
        /// The cell grid of the game.
        /// </summary>
        public CellGrid Grid { get { return grid; } }
        /// <summary>
        /// The ruleset for the game.
        /// </summary>
        public Ruleset Rules { get; private set; }
        /// <summary>
        /// The number of cells currently alive.
        /// </summary>
        public int AliveCount { get; private set; }
        /// <summary>
        /// The maximal number of cells that were alive at the same time.
        /// </summary>
        public int MaximalPopulation { get; private set; }
        /// <summary>
        /// The current game cycle.
        /// </summary>
        public int Cycle { get; private set; }

        /// <summary>
        /// Creates a new GOL game.
        /// </summary>
        /// <param name="rows">The number of rows of the cell grid.</param>
        /// <param name="cols">The number of columns of the cell grid.</param>
        public GOLGame(int rows, int cols)
        {
            grid = new CellGrid(rows, cols);
            Rules = Ruleset.Standard;
            AliveCount = 0;
            MaximalPopulation = 0;
        }

        /// <summary>
        /// Creates a new GOL game.
        /// </summary>
        /// <param name="rows">The number of rows of the cell grid.</param>
        /// <param name="cols">The number of columns of the cell grid.</param>
        /// <param name="born">An array defining the possible numbers of neighbours letting a cell to be born.</param>
        /// <param name="survives">An array defining the possible numbers of neighbours letting a cell survive.</param>
        public GOLGame(int rows, int cols, Ruleset rules)
        {
            grid = new CellGrid(rows, cols);
            Rules = rules;
            AliveCount = 0;
            MaximalPopulation = 0;
        }

        #region Static methods

        /// <summary>
        /// Loads a new game from a given configuration file.
        /// </summary>
        /// <param name="filePath">The path to the file holding the configuration.</param>
        /// <returns>The loaded game.</returns>
        public static GOLGame LoadGameFromFile(string filePath)
        {
            string[] lines = File.ReadAllLines(filePath);
            // decode ruleset
            Ruleset rules = Ruleset.FromString(lines[0]);
            // determine the number of columns with the first grid line.
            int colNumber = lines[1].Split('|').Length;
            GOLGame game = new GOLGame(lines.Length-1, colNumber, rules);
            game.initialConfig = new bool[lines.Length-1, colNumber];
            // count the alive cells.
            game.AliveCount = 0;
            for (int i = 0; i < lines.Length-1; i++)
            {
                string[] cols = lines[i+1].Split('|');
                // always check if column numbers are equal.
                if (cols.Length != colNumber)
                    throw new FormatException("Number of columns is not consistent in input file.");
                for (int j = 0; j < cols.Length; j++)
                {
                    // save the status in the grid and in the initial configuration.
                    Cell cell = new Cell(j, i, parseStateChar(cols[j]));
                    game.initialConfig[i, j] = cell.Alive;
                    game.grid[i, j] = cell;
                    if (cell.Alive) game.AliveCount++;
                }
            }
            game.Cycle = 0;
            game.MaximalPopulation = game.AliveCount;
            return game;
        }

        /// <summary>
        /// Parses a cells status char to boolean.
        /// </summary>
        /// <param name="input">The status char of a cell in a file ('X' or '0').</param>
        /// <returns>True if the status was 'X', false if it was '0'.</returns>
        private static bool parseStateChar(string input)
        {
            input = input.Trim();
            if (input == "X") return true;
            else if (input == "0") return false;
            else throw new FormatException("Invalid cell state format.");
        }

        #endregion

        #region Grid configuration methods.

        /// <summary>
        /// Sets the initial configuration of a game.
        /// </summary>
        /// <param name="config">A two-dimensional boolean array with the height and width of the grid.</param>
        public void SetStartConfiguration(bool[,] config)
        {
            if (config.GetUpperBound(0) != grid.Rows-1
                || config.GetUpperBound(1) != grid.Columns-1)
                throw new ArgumentException("Given configuration has not the right dimensions.");
            setConfig(config);
            initialConfig = config;
        }

        /// <summary>
        /// Returns the current status of all cells in the grid as boolean array.
        /// </summary>
        /// <returns>A two-dimensional boolean array holding true if the cell is alive and false if it is not.</returns>
        public bool[,] GetCurrentConfiguration()
        {
            bool[,] config = new bool[Grid.Rows, Grid.Columns];
            for (int i = 0; i < Grid.Rows; i++)
            {
                for (int j = 0; j < Grid.Columns; j++)
                {
                    config[i, j] = Grid[i, j].Alive;
                }
            }
            return config;
        }

        /// <summary>
        /// Saves the current configuration of all cells to a given file.
        /// </summary>
        /// <param name="filePath">The path of the file where to save the configuration.</param>
        public void WriteStateToFile(string filePath)
        {
            string[] lines = new string[grid.Rows+1];
            lines[0] = Rules.ToString();
            for (int i = 0; i < grid.Rows; i++)
            {
                // Collect all cells of a row in a list.
                List<string> sequence = new List<string>(grid.Columns);
                for (int j = 0; j < grid.Columns; j++)
                {
                    // Translate the cell status to the right string symbol.
                    sequence.Add(grid[i, j].Alive ? " X " : " 0 ");
                }
                lines[i+1] = String.Join("|", sequence);
            }
            File.WriteAllLines(filePath, lines);
        }

        /// <summary>
        /// Resets the game to its initial configuration.
        /// </summary>
        public void ResetConfiguration()
        {
            if (initialConfig == null)
                throw new InvalidOperationException("No start configuration set.");
            setConfig(initialConfig);
        }

        /// <summary>
        /// Applies the given configuration to the game.
        /// </summary>
        /// <param name="config">The grid configuration as two-dimensional boolean array.</param>
        private void setConfig(bool[,] config)
        {
            // Reset alive count and cycle count (at the end).
            AliveCount = 0;
            for (int row = 0; row < grid.Rows; row++)
            {
                for (int col = 0; col < grid.Columns; col++)
                {
                    grid[row, col] = new Cell(col, row, config[row, col]);
                    if (config[row, col]) AliveCount++;
                }
            }
            MaximalPopulation = AliveCount;
            Cycle = 0;
        }

        #endregion

        #region Game mechanics

        /// <summary>
        /// Indicates whether the cell at the given row and column is alive.
        /// </summary>
        /// <param name="row">The row of the cell.</param>
        /// <param name="col">The column of the cell.</param>
        /// <returns>True if the cell is alive, false if not.</returns>
        public bool CellAlive(int row, int col)
        {
            // Take the mathematical modulo so that boundary conditions are met:
            // E.g. if the given position is lower than the left bound, a cell from the right bound is accessed.
            // This is used for accessing a cell's neighbours.
            row = ((row % grid.Rows) + grid.Rows) % grid.Rows;
            col = ((col % grid.Columns) + grid.Columns) % grid.Columns;
            return grid[row, col].Alive;
        }

        /// <summary>
        /// Calculate and apply the next game cycle.
        /// </summary>
        public void NextCycle()
        {
            AliveCount = 0;
            // Collect the update actions for all cells to apply them simultaneously
            List<Action> updateActions = new List<Action>();
            for (int row = 0; row < grid.Rows; row++)
            {
                for (int col = 0; col < grid.Columns; col++)
                    updateActions.Add(setCellState(row, col));
            }
            // Run all update actions in parallel.
            Parallel.Invoke(updateActions.ToArray());
            // Set the maximal population if needed.
            MaximalPopulation = Math.Max(AliveCount, MaximalPopulation);
            Cycle++;
        }

        /// <summary>
        /// Determine the next state of the cell at the given location.
        /// </summary>
        /// <param name="row">The row of the cell.</param>
        /// <param name="col">The column of the cell.</param>
        /// <returns>An action that applies the determined new state to the cell.</returns>
        private Action setCellState(int row, int col)
        {
            // Count the neighbours that are alive.
            byte count = 0;
            for (int i = -1; i < 2; i++)
            {
                for (int j = -1; j < 2; j++)
                {
                    if (!(j == 0 && i == 0))
                    {
                        count += CellAlive(row+i, col+j) ? (byte)1 : (byte)0;
                    }
                }
            }
            // Search the counted value in the arrays defining the rules to determine the next state.
            if (!grid[row, col].Alive && Array.IndexOf(Rules.BornRules, count) > -1)
            {
                AliveCount++;
                return new Action(() => { grid[row, col].SetAlive(true); });
            }
            else if (grid[row, col].Alive && Array.IndexOf(Rules.SurviveRules, count) > -1)
            {
                AliveCount++;
                return new Action(() => { grid[row, col].SetAlive(true); });
            }
            else
                return new Action(() => { grid[row, col].SetAlive(false); });
        }

        #endregion
    }
}
