﻿using System;

namespace GameOfLife
{
    /// <summary>
    /// Represents the ruleset of a Game of Life game.
    /// </summary>
    public class Ruleset
    {
        // The next two arrays define the rules of the game:
        // If a dead cell's number of alive neighbours is in this array, it gets alive.
        public byte[] BornRules { get; private set; }
        // If a cell's number of alive neighbours is in this array, it survives.
        public byte[] SurviveRules { get; private set; }

        /// <summary>
        /// Creates a new ruleset with the given rules.
        /// </summary>
        /// <param name="born">The rules for cells to be born.</param>
        /// <param name="survives">The rules for cells to survive.</param>
        public Ruleset(byte[] born, byte[] survives)
        {
            BornRules = born;
            SurviveRules = survives;
        }

        public override string ToString()
        {
            string bornRules = String.Join("", BornRules);
            string surviveRules = String.Join("", SurviveRules);
            return String.Format("B{0}/S{1}", bornRules, surviveRules);
        }

        /// <summary>
        /// Returns the standard Game of Life rules.
        /// </summary>
        public static Ruleset Standard
        {
            get { return new Ruleset(new byte[] { 3 }, new byte[] { 2, 3 }); }
        }

        /// <summary>
        /// Converts a string encoded ruleset to Ruleset object.
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static Ruleset FromString(string s)
        {
            string[] split = s.Split('/');
            byte[] born = Array.ConvertAll(split[0].Remove(0, 1).ToCharArray(), c => (byte)(c-48));
            byte[] survive = Array.ConvertAll(split[1].Remove(0, 1).ToCharArray(), c => (byte)(c-48));
            return new Ruleset(born, survive);
        }
    }
}
