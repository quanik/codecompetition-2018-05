﻿using System.Collections;

namespace GameOfLife
{
    /// <summary>
    /// Represents the grid of cells in a GOL game.
    /// </summary>
    public class CellGrid : IEnumerable
    {
        private Cell[,] cells;

        /// <summary>
        /// The number of rows of the grid.
        /// </summary>
        public int Rows { get { return cells.GetLength(0); } }
        /// <summary>
        /// The number of columns of the grid.
        /// </summary>
        public int Columns { get { return cells.GetLength(1); } }

        public CellGrid(int rows, int cols)
        {
            cells = new Cell[rows, cols];
        }

        /// <summary>
        /// Returns the grid cell at the specified location.
        /// </summary>
        /// <param name="row">The row in the grid.</param>
        /// <param name="col">The column in the grid.</param>
        /// <returns>The cell at the specified location.</returns>
        public Cell this[int row, int col]
        {
            get { return cells[row, col]; }
            set { cells[row, col] = value; }
        }

        public IEnumerator GetEnumerator()
        {
            for (int row = 0; row < Rows; row++)
            {
                for (int col = 0; col < Columns; col++)
                {
                    yield return cells[row, col];
                }
            }
        }
    }
}
