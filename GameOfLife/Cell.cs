﻿namespace GameOfLife
{
    /// <summary>
    /// Represents a cell in the grid.
    /// </summary>
    public class Cell
    {
        /// <summary>
        /// The x-coordinate of the position of the cell.
        /// </summary>
        public int X { get; private set; }
        /// <summary>
        /// The y-coordinate of the position of the cell.
        /// </summary>
        public int Y { get; private set; }
        /// <summary>
        /// Indicates whether the cell is currently alive.
        /// </summary>
        public bool Alive { get; private set; }
        /// <summary>
        /// Indicates whether the cell has been alive before.
        /// </summary>
        public bool WasAlive { get; private set; }
        /// <summary>
        /// The current age of the cell (number of generation it is alive).
        /// </summary>
        public int Age { get; private set; }
        /// <summary>
        /// Indicates whether the cell was born in this cycle.
        /// </summary>
        public bool Born { get; private set; }
        /// <summary>
        /// Indicates whether the cell died in this cycle.
        /// </summary>
        public bool Died { get; private set; }

        /// <summary>
        /// Creates a new cell.
        /// </summary>
        /// <param name="x">The x-coordinate of the cell's position.</param>
        /// <param name="y">The y-coordinate of the cell's position.</param>
        /// <param name="alive">The initial alive state.</param>
        public Cell(int x, int y, bool alive)
        {
            X = x;
            Y = y;
            Alive = alive;
            WasAlive = alive;
        }

        /// <summary>
        /// Sets the alive state of the cell.
        /// </summary>
        /// <param name="newAlive">true if the cell should be alive, otherwise false.</param>
        public void SetAlive(bool newAlive)
        {
            Age = (Alive && newAlive) ? Age + 1 : 0;
            Born = !Alive && newAlive;
            Died = Alive && !newAlive;
            WasAlive = WasAlive || newAlive;
            Alive = newAlive;
        }

        public override string ToString()
        {
            return string.Format("{0};{1};{2}", X, Y, Alive);
        }
    }
}
