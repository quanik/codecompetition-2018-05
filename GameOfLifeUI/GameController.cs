﻿using GameOfLife;
using System;
using System.ComponentModel;
using System.Windows.Threading;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace GameOfLifeUI
{
    public class GameController : INotifyPropertyChanged
    {
        // The bitmap where to draw the grid.
        public WriteableBitmap WriteBmp { get; private set; }
        // The size of a cell on the bitmap.
        private int cellScale = 5;
        // see EditMode
        private bool editMode = false;
        // see Running
        private bool running = false;
        // The timer for animation.
        private DispatcherTimer timer;

        #region Public properties

        /// <summary>
        /// Holds the current game.
        /// </summary>
        public GOLGame Game { get; private set; }
        /// <summary>
        /// The animation interval in milliseconds.
        /// </summary>
        public int TimerInterval { get; set; }
        /// <summary>
        /// The coloring of the cells in different states.
        /// </summary>
        public Color[] CellColors { get; set; }
        /// <summary>
        /// Indicates whether the editing mode is currently opened.
        /// </summary>
        public bool EditMode
        {
            get { return editMode; }
            set
            {
                editMode = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(EditMode)));
            }
        }
        /// <summary>
        /// Indicates whether the game animation is running.
        /// </summary>
        public bool Running
        {
            get { return running; }
            set
            {
                running = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Running)));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        public GameController()
        {
            // Load cell colors from settings.
            CellColors = new Color[] { Properties.Settings.Default.DeadColor, Properties.Settings.Default.DyingColor,
                                       Properties.Settings.Default.AliveColor, Properties.Settings.Default.BornColor};
            // Initialize timer.
            TimerInterval = Properties.Settings.Default.Interval;
            timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromMilliseconds(TimerInterval);
            timer.Tick += timer_Tick;
        }

        #region Game control methods

        /// <summary>
        /// Starts the game animation.
        /// </summary>
        public void StartAnimation()
        {
            timer.Interval = TimeSpan.FromMilliseconds(TimerInterval);
            timer.Start();
            Running = true;
        }

        /// <summary>
        /// Stops the game animation.
        /// </summary>
        public void StopAnimation()
        {
            timer.Stop();
            Running = false;
        }

        /// <summary>
        /// Resets the game.
        /// </summary>
        public void Reset()
        {
            Game.ResetConfiguration();
            Update();
        }

        /// <summary>
        /// Updates the game area.
        /// </summary>
        public void Update()
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(null));
            redrawBitmap();
        }

        /// <summary>
        /// Sets a new game.
        /// </summary>
        /// <param name="game"></param>
        public void SetNewGame(GOLGame game)
        {
            Game = game;
            // Set new bitmap for drawing.
            WriteBmp = BitmapFactory.New(Game.Grid.Columns * cellScale, Game.Grid.Rows * cellScale);
            Update();
        }

        /// <summary>
        /// Calculates the next game cycle and updates the window.
        /// </summary>
        public void CalculateCycle()
        {
            Game.NextCycle();
            Update();
        }

        private void timer_Tick(object sender, EventArgs e) => CalculateCycle();

        /// <summary>
        /// Redraws the grid on the bitmap.
        /// </summary>
        private void redrawBitmap()
        {
            WriteBmp.Clear(Colors.White);
            foreach (Cell cell in Game.Grid)
            {
                Color col;
                if (cell.Born) col = CellColors[3];
                else if (cell.Alive) col = CellColors[2];
                else if (cell.Died) col = CellColors[1];
                else col = CellColors[0];
                int x = cell.X * cellScale;
                int y = cell.Y * cellScale;
                WriteBmp.FillRectangle(x, y, x + cellScale, y + cellScale, col);
            }
        }

        #endregion
    }
}
