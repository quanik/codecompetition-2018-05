﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace GameOfLifeUI
{
    public partial class SettingsWindow : Window
    {
        private GameController controller;

        public SettingsWindow(MainWindow owner)
        {
            InitializeComponent();
            this.Owner = owner;
            controller = ((MainWindow)this.Owner).Controller;
            this.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            // Set current data.
            txtInterval.Text = controller.TimerInterval.ToString();
            lblRules.Content = controller.Game.Rules.ToString();
            butDead.Background = new SolidColorBrush(controller.CellColors[0]);
            butDied.Background = new SolidColorBrush(controller.CellColors[1]);
            butAlive.Background = new SolidColorBrush(controller.CellColors[2]);
            butBorn.Background = new SolidColorBrush(controller.CellColors[3]);
        }

        private void butSubmit_Click(object sender, RoutedEventArgs e)
        {
            if (!int.TryParse(txtInterval.Text, out int result))
            {
                MessageBox.Show("Die Eingabe für die Animationsgeschwindigkeit ist ungültig.", "Fehler");
                return;
            }
            // Set new data.
            controller.TimerInterval = result;
            controller.CellColors[0] = ((SolidColorBrush)butDead.Background).Color;
            controller.CellColors[1] = ((SolidColorBrush)butDied.Background).Color;
            controller.CellColors[2] = ((SolidColorBrush)butAlive.Background).Color;
            controller.CellColors[3] = ((SolidColorBrush)butBorn.Background).Color;
            this.DialogResult = true;
            this.Close();
        }

        private void butCancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }

        private void butColor_Click(object sender, RoutedEventArgs e)
        {
            // Use Color Dialog of Windows Forms and convert Color object
            System.Windows.Forms.ColorDialog colD = new System.Windows.Forms.ColorDialog();
            colD.FullOpen = true;
            if (colD.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                Color newColor = Color.FromRgb(colD.Color.R, colD.Color.G, colD.Color.B);
                ((Button)e.Source).Background = new SolidColorBrush(newColor);
            }
        }
    }
}
