﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls.Primitives;
using GameOfLife;
using Microsoft.Win32;
using Bluegrams.Application.WPF;

namespace GameOfLifeUI
{
    public partial class MainWindow : Window
    {
        // Manager saving settings.
        private MiniAppManager manager;
        // An array holding the grid configuration for editing.
        private bool[,] editConfig;

        /// <summary>
        /// The game controller.
        /// </summary>
        public GameController Controller { get; private set; }

        #region Window methods

        public MainWindow()
        {
            // Initialize manager.
            manager = new MiniAppManager(this, true);
            manager.MakePortable(Properties.Settings.Default);
            manager.Initialize();
            InitializeComponent();
            // Initialize controller.
            Controller = new GameController();
            this.DataContext = Controller;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            StartWindow startWin = new StartWindow(this, "Examples/Octagon.txt");
            if (startWin.ShowDialog() == true)
                setNewGame(startWin.LoadedGame);
            else
            {
                // Disable buttons if no game loaded.
                menuAnimation.IsEnabled = false;
                butEdit.IsEnabled = false;
                butSave.IsEnabled = false;
                MessageBox.Show(this, "Lade ein neues Spiel um zu starten.");
            }
        }

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            // Save the currently choosen settings.
            Properties.Settings.Default.DeadColor = Controller.CellColors[0];
            Properties.Settings.Default.DyingColor = Controller.CellColors[1];
            Properties.Settings.Default.AliveColor = Controller.CellColors[2];
            Properties.Settings.Default.BornColor = Controller.CellColors[3];
            Properties.Settings.Default.Interval = Controller.TimerInterval;
            Properties.Settings.Default.Save();
        }

        #endregion

        #region New Game, Save Game, Settings

        private void butNew_Click(object sender, RoutedEventArgs e)
        {
            StartWindow startWin = new StartWindow(this);
            if (startWin.ShowDialog() == true)
                setNewGame(startWin.LoadedGame);
        }

        private void butSave_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Title = "Konfiguration speichern";
            sfd.InitialDirectory = Environment.CurrentDirectory;
            if (sfd.ShowDialog() == true)
            {
                Controller.Game.WriteStateToFile(sfd.FileName);
            }
        }

        private void butSettings_Click(object sender, RoutedEventArgs e)
        {
            SettingsWindow settingsWin = new SettingsWindow(this);
            if (settingsWin.ShowDialog() == true)
                Controller.Update();
        }

        private void setNewGame(GOLGame game)
        {
            Controller.SetNewGame(game);
            imgMain.Source = Controller.WriteBmp;
            // Enable menu buttons.
            menuAnimation.IsEnabled = true;
            butEdit.IsEnabled = true;
            butSave.IsEnabled = true;
        }

        #endregion

        #region Game control event handlers

        private void butStart_Click(object sender, RoutedEventArgs e) => Controller.StartAnimation();

        private void butStop_Click(object sender, RoutedEventArgs e) => Controller.StopAnimation();

        private void butReset_Click(object sender, RoutedEventArgs e) => Controller.Reset();

        private void butStep_Click(object sender, RoutedEventArgs e) => Controller.CalculateCycle();

        #endregion

        #region Edit Grid

        private void butEdit_Click(object sender, RoutedEventArgs e)
        {
            if (butEdit.IsChecked.Value) startEditing();
            else
            {
                // Clear editing configuration and editing control if finished.
                editConfig = null;        
                cvMain.Items.Clear();
            }
        }

        private void startEditing()
        {
            // Update the editing configuration to the current grid.
            editConfig = Controller.Game.GetCurrentConfiguration();
            // Add a button for each cell.
            foreach (Cell cell in Controller.Game.Grid)
            {
                cvMain.Items.Add(cell);
            }
        }

        private void butCheck_Click(object sender, RoutedEventArgs e)
        {
            ToggleButton senderBut = (ToggleButton)sender;
            // Determine the position of the cell represented by this button.
            string[] tag = senderBut.Tag.ToString().Split('x');
            int row = int.Parse(tag[0]);
            int col = int.Parse(tag[1]);
            editConfig[row, col] = senderBut.IsChecked.Value;
        }

        private void butFinishEdit_Click(object sender, RoutedEventArgs e)
        {
            // Set a new game with edited grid.
            GOLGame game = new GOLGame(editConfig.GetLength(0), editConfig.GetLength(1), Controller.Game.Rules);
            game.SetStartConfiguration(editConfig);
            setNewGame(game);
            butEdit.IsChecked = false;
        }

        private void butCancelEdit_Click(object sender, RoutedEventArgs e) => butEdit.IsChecked = false;

        #endregion
    }
}
