﻿using GameOfLife;
using Microsoft.Win32;
using System;
using System.Windows;

namespace GameOfLifeUI
{
    public partial class StartWindow : Window
    {
        /// <summary>
        /// Indicates whether the new game should be loaded from file.
        /// </summary>
        public bool FromFile { get; set; } = true;
        /// <summary>
        /// The loaded game after submit.
        /// </summary>
        public GOLGame LoadedGame { get; private set; }

        public StartWindow(Window owner) : this(owner, "") { }

        public StartWindow(Window owner, string presetFile)
        {
            InitializeComponent();
            this.Owner = owner;
            this.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            this.DataContext = this;
            if (!String.IsNullOrEmpty(presetFile)) loadFromFile(presetFile);
        }

        private void butFromFile_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Title = "Startkonfiguration laden";
            ofd.InitialDirectory = Environment.CurrentDirectory;
            if (ofd.ShowDialog() == true)
            {
                loadFromFile(ofd.FileName);
            }
        }

        private void butSubmit_Click(object sender, RoutedEventArgs e)
        {
            Ruleset rules = parseRuleset();
            if (rules == null) return;
            if (FromFile && LoadedGame == null)
            {
                MessageBox.Show("Keine Datei angegeben.", "Fehler");
                return;
            }
            else if (!FromFile && !loadNew(rules)) return;
            this.DialogResult = true;
            this.Close();
        }

        /// <summary>
        /// Loads a new game from file.
        /// </summary>
        /// <returns>True if loading was successful, false otherwise.</returns>
        private bool loadFromFile(string filePath)
        {
            lblFile.Content = "";
            if (!System.IO.File.Exists(filePath))
            {
                MessageBox.Show("Die angegebene Datei existiert nicht.", "Fehler");
                LoadedGame = null;
                return false;
            }
            try
            {
                LoadedGame = GOLGame.LoadGameFromFile(filePath);
            }
            catch
            {
                MessageBox.Show("Die angegebene Datei hat das falsche Format.", "Fehler");
                LoadedGame = null;
                return false;
            }
            lblFile.Content = System.IO.Path.GetFileName(filePath);
            txtBorn.Text = String.Join(",", LoadedGame.Rules.BornRules);
            txtSurvive.Text = String.Join(",", LoadedGame.Rules.SurviveRules);
            return true;
        }

        /// <summary>
        /// Creates a new game with the given dimensions.
        /// </summary>
        /// <returns>True if creation was successful, false otherwise.</returns>
        private bool loadNew(Ruleset rules)
        {
            int rows, cols;
            try
            {
                rows = int.Parse(txtRows.Text);
                cols = int.Parse(txtCols.Text);
                if (rows <= 0 || cols <= 0 || rows * cols > 100000) throw new FormatException();
            }
            catch (FormatException)
            {
                MessageBox.Show("Die Eingabe für die Gittergröße ist ungültig oder zu groß.", "Fehler");
                return false;
            }
            bool[,] config = new bool[rows, cols];
            LoadedGame = new GOLGame(rows, cols, rules);
            LoadedGame.SetStartConfiguration(config);
            return true;
        }

        // Parses the content of the ruleset textboxes to a Ruleset object.
        private Ruleset parseRuleset()
        {
            byte[] bornRules, surviveRules;
            try
            {
                bornRules = Array.ConvertAll(txtBorn.Text.Split(','), byte.Parse);
                surviveRules = Array.ConvertAll(txtSurvive.Text.Split(','), byte.Parse);
            }
            catch (FormatException)
            {
                MessageBox.Show("Das eingegebene Regelset hat das falsche Format.", "Fehler");
                return null;
            }
            return new Ruleset(bornRules, surviveRules);
        }

        private void butCancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }
    }
}
