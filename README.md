# Game Of Life 2018

## Einf�hrung
Dies ist eine Implementation von *Conway's Game Of Life* in C#. Das Projekt besteht einmal aus
einer .NET Standard/ .NET Core Library, die die Grundfunktionen des Spiels implementiert.
Auf ihr k�nnen unterschiedliche (grafische) Anwendungen aufbauen, realisiert ist eine
Windows Desktop-Anwendung mit WPF.

Das Projekt ist zu finden auf Bitbucket unter https://bitbucket.org/quanik/gameoflife2018.

## Systemvoraussetzungen
* Windows 7 oder h�her
* .NET Framework v.4.6.1 oder h�her

## Setup
1. Herunterladen der vorkompilierten Version unter https://bitbucket.org/quanik/gameoflife2018/downloads/.
2. Extrahieren des heruntergeladenen Ordners.
3. Das Programm (Datei *GameOfLifeUI.exe*) ist direkt startbereit.

## Features
* Speichern und Laden von Spielkonfigurationen aus Dateien
* Grafisches Editieren der Besetzung des Spielfelds
* Grafische Animation des Spielverlaufs
* Einstellbare Animationsgeschwindigkeit und schrittweiser Ablauf
* Darstellung der Zellen in bis zu vier verschiedenen Farben
* Spielen mit allen m�glichen alternativen Regelsets

## Hilfe
Unter https://bitbucket.org/quanik/gameoflife2018/wiki/Home findet sich eine kurze Anleitung zur Bedienung der Anwendung.

## Anmerkungen
Game Of Life nutzt die Arbeiten von folgenden Projekten:

* **WriteableBitmapEx**, Copyright (c) 2009-2015 Rene Schulte,
	unter MIT-Lizenz von https://github.com/teichgraf/WriteableBitmapEx.

* **MiniAppManager**, Copyright (c) 2018, Bluegrams,
	unter BSD-3-Clause von https://github.com/bluegrams/MiniAppManager.

* Die verwendeten Icons stammen von [Just Icon](https://www.iconfinder.com/justicon)
	von https://www.iconfinder.com/ und sind frei verf�gbar.