using Microsoft.VisualStudio.TestTools.UnitTesting;
using GameOfLife;

namespace GameOfLifeTests
{
    [TestClass]
    public class SimulationTests
    {
        GOLGame game;
        bool[,] config;

        [TestInitialize]
        public void TestInit()
        {
            game = new GOLGame(6, 5);
            config = new bool[,] { { false, true, false, false, false },
                                 { true, false, false, true, false },
                                 { false, false, false, false, true },
                                 { false, true, false, false, true },
                                 { true, false, false, false, false },
                                 { false, false, false, false, false } };
        }

        // Test members: Rows, Columns, Cycle.
        [TestMethod]
        public void TestProperties()
        {
            Assert.AreEqual(6, game.Grid.Rows);
            Assert.AreEqual(5, game.Grid.Columns);
            game.SetStartConfiguration(config);
            game.NextCycle();
            game.NextCycle();
            Assert.AreEqual(2, game.Cycle);
        }

        // Test members: SetStartConfiguration, CellAlive, AliveCount.
        [TestMethod]
        public void TestStartConfiguration()
        {     
            game.SetStartConfiguration(config);
            Assert.AreEqual(7, game.AliveCount);
            Assert.IsTrue(game.CellAlive(0, 1));
            Assert.IsTrue(game.CellAlive(3, 4));
            Assert.IsTrue(game.CellAlive(4, 0));
            Assert.IsFalse(game.CellAlive(1, 1));
            Assert.IsFalse(game.CellAlive(2, 3));
        }

        // Test members: NextCycle, AliveCount, Cell.Died, Cell.Born.
        [TestMethod]
        public void TestNextCycle()
        {
            bool[,] nextConfig = { { false, false, false, false, false },
                                    { true, false, false, false, true },
                                    { false, false, false, true, true },
                                    { false, false, false, false, true },
                                    { true, false, false, false, false },
                                    { false, false, false, false, false } };
            game.SetStartConfiguration(config);
            game.NextCycle();
            Assert.AreEqual(6, game.AliveCount);
            for (int row = 0; row <= nextConfig.GetUpperBound(0); row++)
            {
                for (int col = 0; col <= nextConfig.GetUpperBound(1); col++)
                {
                    Assert.AreEqual(nextConfig[row, col], game.Grid[row, col].Alive, string.Format("Position: {0}; {1}.", row, col));
                }
            }
            Assert.IsTrue(game.Grid[0, 1].Died);
            Assert.IsTrue(game.Grid[1, 3].Died);
            Assert.IsTrue(game.Grid[1, 4].Born);
            Assert.IsTrue(game.Grid[2, 3].Born);
            Assert.IsTrue(game.Grid[3, 1].Died);
        }

        // Test members: NextCycle, Cell.WasAlive, Cell.Age.
        [TestMethod]
        public void TestThirdCycle()
        {
            bool[,] nextConfig = { { false, false, false, false, true },
                                    { false, false, false, true, true },
                                    { false, false, true, false, false },
                                    { false, false, false, true, true },
                                    { false, false, false, false, true },
                                    { false, false, false, false, false } };
            game.SetStartConfiguration(config);
            game.NextCycle();
            game.NextCycle();
            game.NextCycle();
            Assert.AreEqual(7, game.AliveCount);
            for (int row = 0; row <= nextConfig.GetUpperBound(0); row++)
            {
                for (int col = 0; col <= nextConfig.GetUpperBound(1); col++)
                {
                    Assert.AreEqual(nextConfig[row, col], game.Grid[row, col].Alive, string.Format("Position: {0}; {1}.", row, col));
                }
            }
            Assert.IsTrue(game.Grid[0, 1].WasAlive);
            Assert.IsTrue(game.Grid[2, 4].WasAlive);
            Assert.IsFalse(game.Grid[1, 1].WasAlive);
            Assert.AreEqual(0, game.Grid[2, 3].Age);
            Assert.AreEqual(2, game.Grid[1, 4].Age);
            Assert.AreEqual(1, game.Grid[3, 3].Age);
        }

        // Test members: LoadGameFromFile.
        [TestMethod]
        public void TestLoadGameFromFile()
        {
            game = GOLGame.LoadGameFromFile("TestConfig.txt");
            Assert.AreEqual(10, game.Grid.Columns);
            Assert.AreEqual(10, game.Grid.Rows);
            Assert.AreEqual(24, game.AliveCount);
            bool[] vert = new bool[] { false, false, false, true, false, false, true, false, false, false };
            bool[] horiz = new bool[] { false, true, true, false, true, true, false, true, true, false };
            testLine(game, 1, vert);
            testLine(game, 2, vert);
            testLine(game, 3, horiz);
            testLine(game, 4, vert);
            testLine(game, 5, vert);
            testLine(game, 6, horiz);
            testLine(game, 7, vert);
            testLine(game, 8, vert);
        }

        // Tests if the specified line in the games grid matches the given configuration.
        private void testLine(GOLGame game, int line, bool[] sequence)
        {
            for (int i = 0; i < sequence.Length; i++)
            {
                Assert.AreEqual(sequence[i], game.Grid[line, i].Alive);
            }
        }

        // Test members: NextCycle with  rules B1/S12.
        [TestMethod]
        public void TestAlternativeRules()
        {
            Ruleset rules = new Ruleset(new byte[] { 1 }, new byte[] { 1, 2 });
            GOLGame game = new GOLGame(5, 5, rules);
            bool[,] startConfig = { { false, false, false, false, false },
                               { false, false, false, false, false },
                               { false, false, true, false, false },
                               { false, false, false, false, false },
                                { false, false, false, false, false }};
            bool[,] newConfig = { { true, false, false, false, true },
                                  { false, true, false, true, false },
                                  { false, false, false, false, false },
                                  { false, true, false, true, false },
                                  { true, false, false, false, true }};
            game.SetStartConfiguration(startConfig);
            game.NextCycle();
            game.NextCycle();
            Assert.AreEqual(8, game.AliveCount);
            for (int i = 0; i < newConfig.GetLength(0); i++)
            {
                for (int j = 0; j < newConfig.GetLength(1); j++)
                {
                    Assert.AreEqual(newConfig[i, j], game.CellAlive(i, j));
                }
            }
        }
    }
}
